#ifndef ALGORITHM_COUNT_DIGITS_H
#define ALGORITHM_COUNT_DIGITS_H

namespace alg {
    static unsigned const PowersOf10[] =
            {1, 10, 100, 1000, 10000, 100000,
             1000000, 10000000, 100000000, 1000000000};

    unsigned numDigits(unsigned v) {
        auto log2 = 31 - __builtin_clz(v);
        auto log10Approx = (log2 + 1) * 1233 >> 12;
        auto log10 = log10Approx - (v < PowersOf10[log10Approx]);
        return log10 + 1;
    }
}

#endif //ALGORITHM_COUNT_DIGITS_H
