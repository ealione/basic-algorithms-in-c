#ifndef ALGORITHM_FAST_INVERSE_SQRT_H
#define ALGORITHM_FAST_INVERSE_SQRT_H

//http://h14s.p5r.org/2012/09/0x5f3759df.html
namespace alg {
    float FastInvSqrt(float x) {
        float xhalf = 0.5f * x;
        int i = *(int *) &x;         // evil floating point bit level hacking
        i = 0x5f3759df - (i >> 1);  // what the fuck?
        x = *(float *) &i;
        x = x * (1.5f - (xhalf * x * x));
        return x;
    }
}

#endif //ALGORITHM_FAST_INVERSE_SQRT_H
