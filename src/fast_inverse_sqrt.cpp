#include <stdio.h>
#include "fast_inverse_sqrt.h"

int main()
{
    using namespace alg;
    int MAXELEMENTS=10;

    int i;
    int num = 100;
    for (i=1;i <= MAXELEMENTS; i++) {
        num = i * num;
        auto dig = FastInvSqrt(num);
        printf("number:%d->invsqrt:%f\n", num, dig);
    }
    return 0;
}
