#include <stdio.h>
#include "count_digits.h"

int main()
{
    using namespace alg;
    int MAXELEMENTS=10;

    int i;
    int num = 100;
    for (i=1;i <= MAXELEMENTS; i++) {
        num = i * num;
        auto dig = numDigits(num);
        printf("number:%d->digits:%d\n", num, dig);
    }
    return 0;
}
